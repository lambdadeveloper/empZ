# Formulário de pré-entrevista empZ

## Questão nº1 - Construa uma classe PHP com atributos, construtor e funções (métodos). Descrever um exemplo de cada no mínimo.

```php
<?php

namespace Entrevista;
// Visando agrupamento e organização do código.

/**
 * Avaliação empZ - 1ª pergunta
 * A presente classe simula um tratamento de rotas para requisições web.
 *
 * @category JOB_Interview
 * @package  EmpZ
 * @author   John Murowaniecki <john@compilou.com.br>
 * @license  Creative Commons 4.0 - Some rights reserved
 * @link     https://gitlab.com/php-developer/empZ
 */
class λ    // Nome da classe Lambda
{

    /**
     * Atributos privados da classe.
     */
    private $ROTAS = [];

    private $URI = '';

    /**
     * Função construtora da classe.
     *
     * A ideia era de que, ao inicializar a classe, o construtor
     * carregasse os dados globais ao funcionamento das rotas e views.
     *
     * @param String $URI texto na íntegra ou parcial a URL solicitada.
     */
    public function __construct(String $URI)
    {
        $this->ROTAS = [];
        $this->URI = $URI;
    }


    /**
     * Função destrutora da classe.
     *
     * Já a ideia pra função destrutora era de que ela quem deveria
     * rendenizar o conteúdo (seja da URI requisitada ou de um alerta).
     */
    public function __destruct()
    {
        $existe = $this->ROTAS[$this->URI] ?? false;
        $funcao = $existe ? [$existe, 0]:[$this->URI, 1];
        \Entrevista\λ::echo($funcao  [0], $funcao[1]);
        // ^ nosso método de impressão é um método estático.
    }


    /**
     * Define todas e armazena as mensagens.
     *
     * A ideia para as rotas era de que deveria ser possível alimentar
     * de forma sequencial a tabela de URIs sem a necessidade de
     * instanciar novamente algum elemento - utilizando o próprio
     * retorno da última função executada para realizar um `pipe`.
     *
     * @param  string $rota  Rota requisitada.
     * @param  [type] $dados Função ou conteúdo a ser devolvido.
     * @return Class         Self
     */
    public function route(String $rota = '', $dados)
    {
        $this->ROTAS[$rota] = is_array($dados) ? implode(' ', $dados) : $dados;
        return $this;
    }

    /**
     * Exibe o conteúdo.
     * @param  string $message Rota requisitada.
     * @param  [type] $codigo  Função ou conteúdo a ser devolvido.
     * @param  [type] $padrao  Armazena cabeçalhos padrão protocolo HTTP.
     * @return Class           Self
     */
    public static function echo($message, $codigo, Array $padrao = [
        '200 OK',
        '404 Page not found'
    ])
    {
        header("HTTP/1.1 $codigo {$padrao[$codigo]}");
        echo(! $codigo?$message:"{$padrao[$codigo]}: $message");
    }
}

// Possivelmente essa seja a parte mais interessante.
(new class($_SERVER['REQUEST_URI'])
    // ^ declaramos e instanciamos uma classe anônima, passando um
    //   parâmetro como se fosse uma função (anônima o não).

    extends \Entrevista\λ
        // ^ não esquecendo que nossa classe anônima extende nossa
        //   classe λ::Lambda.

        {}) // Perceba que a classe anônima não tem conteúdo algum.

        // Ela funciona devidamente pois herda da classe λ::Lambda
        // todos os métodos que utilizaremos nessa demonstração.


        ->route(    // Executamos o método
            '/',    // passando como parâmetro a URI desejada

                   // E logo a seguir o conteúdo a ser impresso.
            [
            '<h1>Página principal</h1>',
            '<p>Este é apenas um teste para ilustrar como instanciar classes anônimas de forma criativa.</p>',
            '<a href="/sobre">sobre o autor</a>'
        ])

        // Perceba que não `finalizamos a instrução ainda`, logo
        // é possível fazer uso da instâcia retornada pela função
        // para cadastrar quantas rotas mais desejarmos - ou executar
        // quaisquer outros comandos que desejarmos.

        ->route('/sobre', [
            '<p><b>Oi! Eu sou o John</b> - sou pai de primeira viagem e programador experiente.</p>',
            '<a href="https://www.linkedin.com/in/php-developer/" target="_blank">LinkedIn</a>',
            '<a href="https://gitlab.com/php-developer/" target="_blank">GitLab</a>',
            '<a href="https://github.com/jmurowaniecki" target="_blank">GitHub</a>',
            '<a href="https://twitter.com/0xD3C0D3" target="_blank">Twitter</a>'
        ])

        ; // Agora sim, finalizamos o `pipe`.

          // Perceba que em momento algum foi necessário setar o script
          // ativamente para ele imprimir os resultados - isso é feito
          // automaticamente pelo método mágico `__destruct`.
```

## Questão nº2 - SQL

```SQL
SELECT usuario.nome     AS nome_usuario,
       supervisor.nome  AS nome_supervisor,
       perfil.descricao AS descricao_perfil
  FROM usuario
  INNER JOIN supervisor ON supervisor.id_supervisor = usuario.id_supervisor
  INNER JOIN perfil     ON     perfil.id_perfil     = usuario.id_perfil
```

## Questão nº3

$.getJson é um método da biblioteca jQuery que realiza requisições web em busca de resultados formatados em JSON; $.ajax por sua vez permite setar outros tipos de requisição.

## Questão nº4

Supondo que algum script pare de funcionar eu iniciaria o procedimento observando os resultados no console, log e inspetor; após me certificar que estou dentro das boas práticas descritas pelo jslint/jshint começaria a validar os pacotes (inclusive para certificar que a minificação/packing está funcionando corretamente).

## Questão nº5

Explicar de forma simples para um leigo pode levar a pensar que ambos fazem a mesma coisa - e não está de todo errado -, porém são métodos distintos para realizar ações distintas.

O método GET tem baixa capacidade de transmitir informação, ele é limitado no que diz respeito a carga e volume de informação - servindo muito mais para requisitar dados do servidor;

Já o método POST tem alta capacidade de transmissão, podendo carregar volumes de dados dezenas de centenas de vezes maiores que o GET.

Existem outras diferenças como a visibilidade dos pacotes de dados trafegados - que implica em análise em aspectos de segurança e privacidade.

## Questão nº6 - descreva meu conhecimento em MySQL

Tenho amplo conhecimento em MySQL - tanto no quesito codificação em SQL quando configuração, tunning, cluster e balanceamento de serviços. Posso dizer que já usei até bem próximo ao limite - forçando a empregar tecnologias como HAPproxy, e experimentar outras soluções (como MariaDB).

## Questão nº7 - Conhecimentos em frameworks

Dos frameworks em **PHP** eu uso bastante λ::lambda, Laravel/Lumen, CodeIgniter, Drupal, CakePHP, Zend mas tenho experiência em OpenCart, Magento, WordPress, Kohana, Joomla, entre outros.  De **Python** recentemente tenho utilizado Tornado, mas tenho uma longa experiência em Django, flask e Pyramid. Não tenho um framework favorito de **Javascript**, mas já desenvolvi bastante em NodeJS com Express, em Ruby on Rails, no Totvs Protheus, nas SDKs do Android, Arduino, ARParot, ..

## Questão nº8 - Metodologias

Sim, tenho vivência em ágile, scrum e waterfall.

## Questão nº9 - versionamento

Hoje utilizo muito mais o GitLab do que o GitHub e o Bitbucket, no entanto são todos excelentes soluções de integração e controle de versão - entre outros.

## Questão nº10 - Arquitetura de sistemas

Sim, possuo vivência em MVC - assim como HMVC -, RCM, n-Tier, monolíticos, distribuídos, pipes. Sou da opinião de que a melhor arquitetura a ser empregada é sempre a mais simples capaz de se adequar a resolver o problema.

Não existe bala de prata em tecnologia, arquitetura, framework ou metodologia - é sempre todo o conjunto.

## Questão nº11 - Método listagem com CakePHP

Quando eu ainda usava :Cake:, costumava realizar ações similares com `RequestAction(getListSelect)` do controller desejado (nesse caso do UsersController) - no entanto poderia resolver também com o `HTML Helper` ou `URL->Build`.

## Questão nº12 - Conhecimentos VB.NET

Meus conhecimentos na plataforma .NET são bem reduzidos, no entanto programei bastante nos anos 90 com Visual Basic 6 e, nos anos 2000 em ASP clássico.
