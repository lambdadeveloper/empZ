<?php

namespace Entrevista;

/**
 * Avaliação empZ - 1ª pergunta
 * A presente classe simula um tratamento de rotas para requisições web.
 *
 * @category JOB_Interview
 * @package  EmpZ
 * @author   John Murowaniecki <john@compilou.com.br>
 * @license  Creative Commons 4.0 - Some rights reserved
 * @link     https://gitlab.com/php-developer/empZ
 */
class λ
{

    /**
     * Atributos privados da classe.
     */
    private $ROTAS = [];

    private $URI = '';

    /**
     * Função construtora da classe.
     *
     * @param String $URI texto na íntegra ou parcial a URL solicitada.
     */
    public function __construct(String $URI)
    {
        $this->ROTAS = [];
        $this->URI = $URI;
    }


    /**
     * Função destrutora da classe.
     */
    public function __destruct()
    {
        $existe = $this->ROTAS[$this->URI] ?? false;
        $funcao = $existe ? [$existe, 0]:[$this->URI, 1];
        \Entrevista\λ::echo($funcao  [0], $funcao[1]);
    }


    /**
     * Define todas e armazena as mensagens.
     * @param  string $rota  Rota requisitada.
     * @param  [type] $dados Função ou conteúdo a ser devolvido.
     * @return Class         Self
     */
    public function route(String $rota = '', $dados)
    {
        $this->ROTAS[$rota] = is_array($dados) ? implode(' ', $dados) : $dados;
        return $this;
    }

    /**
     * Exibe o conteúdo.
     * @param  string $message Rota requisitada.
     * @param  [type] $codigo  Função ou conteúdo a ser devolvido.
     * @param  [type] $padrao  Armazena cabeçalhos padrão protocolo HTTP.
     * @return Class           Self
     */
    public static function echo($message, $codigo, Array $padrao = ['200 OK', '404 Page not found'])
    {
        header("HTTP/1.1 $codigo {$padrao[$codigo]}");
        echo(! $codigo?$message:"{$padrao[$codigo]}: $message");
    }
}


(new class($_SERVER['REQUEST_URI'])
    extends \Entrevista\λ {})
        ->route('/', [
            '<h1>Página principal</h1>',
            '<p>Este é apenas um teste para ilustrar como instanciar classes anônimas de forma criativa.</p>',
            '<a href="/sobre">sobre o autor</a>'
        ])
        ->route('/sobre', [
            '<p><b>Oi! Eu sou o John</b> - sou pai de primeira viagem e programador experiente.</p>',
            '<a href="https://www.linkedin.com/in/php-developer/" target="_blank">LinkedIn</a>',
            '<a href="https://gitlab.com/php-developer/" target="_blank">GitLab</a>',
            '<a href="https://github.com/jmurowaniecki" target="_blank">GitHub</a>',
            '<a href="https://twitter.com/0xD3C0D3" target="_blank">Twitter</a>'
        ]);
