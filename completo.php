<?php

namespace WEB;

/**
 * Avaliação empZ - 1ª pergunta
 * A presente classe simula um tratamento de rotas para requisições web.
 *
 * @category JOB_Interview
 * @package  EmpZ
 * @author   John Murowaniecki <john@compilou.com.br>
 * @license  Creative Commons 4.0 - Some rights reserved
 * @link     https://gitlab.com/php-developer/empZ
 */
class λ
{
    private $r = [];
    private $v = [];
    private $i = '';

    public function __construct(String $i)
    {
        $this->r = [];
        $this->v = [];
        $this->i = $i;
    }

    public function __destruct()
    {
        $x = $this;
        $k = $x->r[$x->i] ?? false;
        $f = $k ? [$k,0]:[$x->i,1];
        $T = $x->v[  'template'  ];
        
        \WEB\λ::echo($X, $f[1]);
    }

    public function route(String $r = '', $f = __FUNCTION__)
    {
        $this->r[$r] = $f;
        return $this;
    }

    public static function echo($m, $c, $d = [
        '200 OK',
        '404 Page not found']
    )
    {
        header("HTTP/1.1 $c {$d[$c]}");
        echo (!$c ? $m:"{$d[$c]}: $m");
    }

    public function view($name, $structure)
    {
        $this->v[$name] = $structure;
    }
}

class HTML
{
    public function __construct($e, $P)
    {
        $x = [];
        foreach ($P as $p => $v) {
            $x[] = "$p=\"$v\"";
        }
        $P = implode(' ', $x);
        $this->t = "<$e $P>";
    }

    public function __toString()
    {
        return $this->t;
    }

    public static function __callStatic($m, $a)
    {
        $x = [];
        $A = is_array;
        foreach ($A($a) ? $a : [$a] as $i => $v) {
            if ($A($v)) {
                $X = [];
                foreach ($v as $I => $V) {
                    $X[] = "$I=\"$V\"";
                }
                $x[] = implode(' ', $X);
            } elseif (is_numeric($i)) {
                $x[] = "$v=\"$v\"";
            } else {
                $x[] = "$i=\"$v\"";
            }
        }
        $x = implode(' ', $x);
        return "<$m $x>";
    }

    public function __call($m, $a)
    {
        $x = (is_array($a)
            ? implode('', $a)
            : $a);
        return "<$m>$x</$m>";
    }
}


(new class($_SERVER['REQUEST_URI'])
    extends \WEB\λ {})
        ->view('template',
            ($_ = new HTML)
                ->html(
                    $_->head(
                        $_->title('Obrigado e volte logo'),
                        HTML::meta(['charset' => 'utf-8'])
                    ),
                    $_->body(
                        $_->div(
                            $_->h1('Bem vindo!'),
                            $_->h2('Como você está?')
                        ),
                        $_->div('{conteudo}')
                    )
                )
        )
        ->route('/', ['conteudo' => 'main page'])
        ->route('/sobre', ['conteudo' => 'author john']);
